import { useState } from "react";

const PeopleForm = ({ updatePeople }) => {
  const [first, setFirst] = useState("");
  const [age, setAge] = useState(0);

  const handleFirstChange = (event) => setFirst(event.target.value);
  const handleAgeChange = (event) => setAge(Number.parseInt(event.target.value, 10));

  const submitForm = (event) => {
    // stop the native default event from happening
    event.preventDefault();
    updatePeople({
      first,
      age,
    });
    setFirst("");
    setAge(0);
  };

  return (
    <>
      <form onSubmit={submitForm}>
        <input name="first" value={first} type="text" onChange={handleFirstChange} />
        <input name="age" value={age} type="number" onChange={handleAgeChange} />
        <button type="submit">Submit me!</button>
      </form>
    </>
  );
};

export default PeopleForm;
