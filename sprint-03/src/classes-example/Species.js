import React from "react";
import axios from "axios";
import { getIdFromUrl } from "../util";

export class Species extends React.Component {
  state = {
    data: [],
  };

  // usually want to fetch data when mounting
  // componentDidMount() {
  //   fetch("https://swapi.dev/api/species")
  //     .then((response) => response.json())
  //     .then((data) => {
  //       this.setState({ data: data.results });
  //     })
  //     .catch((err) => {
  //       console.error(err);
  //     });
  // }

  async componentDidMount() {
    try {
      // const response = await fetch("https://swapi.dev/api/species");
      // const data = await response.json();
      const { data } = await axios.get("https://swapi.dev/api/species");
      this.setState({ data: data.results });
    } catch (err) {
      console.error("Unable to fetch species", err);
    }
  }

  render() {
    const { data } = this.state;
    return (
      <>
        <ul>
          {data.map((species) => (
            <li key={getIdFromUrl(species.url)}>{species.name}</li>
          ))}
        </ul>
      </>
    );
  }
}
