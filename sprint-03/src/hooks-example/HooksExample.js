import { useEffect, useState } from "react";

export const HooksExample = ({ name }) => {
  const [randomNumber, setRandomNumber] = useState(0);

  useEffect(() => {
    // Everything from here
    console.log(`Hooks ${name} mounted!`);
    // To here will run on "componentDidMount" and ...

    return () => {
      console.log(`Hooks ${name} unmounted!`);
    };
    // eslint-disable-next-line
  }, []);

  useEffect(() => {
    console.log(`randomNumber ${randomNumber} has changed!`);
  }, [randomNumber]);

  useEffect(() => {
    console.log("starting timer");
    const timer = setInterval(() => {
      console.log(`Tick Tock... ${randomNumber}`);
    }, 1000);

    return () => {
      console.log("Cleaning up our timer =]");
      clearInterval(timer);
    };
  }, [randomNumber]);

  // console.log(`Hooks ${name} rendered!`);

  return (
    <>
      <h1>Hooks my name is {name}</h1>
      <button onClick={() => setRandomNumber(Math.random())}>Render</button>
    </>
  );
};
