import { Component, useState } from "react";

/**
 * NEVER EVER DO ANY OF THESE THINGS
 */

// export const Example = () => {
//   const [num, setNum] = useState(0);

//   setNum(num + 1);

//   console.log("rendering.. infinitely......");

//   return (
//     <>
//       <p onClick={() => setNum(num + 1)}>{num}</p>
//     </>
//   );
// };

export class Example extends Component {
  state = {
    num: 0,
  };

  render() {
    console.log("rendering.. infinitely......");
    return (
      <>
        <p onClick={() => this.setState({ num: this.state.num + 1 })}>{this.state.num}</p>
      </>
    );
  }
}
