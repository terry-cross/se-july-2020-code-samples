class Counter extends React.Component {
  /*
  OLD WAY - NOT COOL
  constructor(props) {
    super(props);
    this.state = {
      count: props.initialCount,
      name: "Vince",
    };
    this.increment = this.increment.bind(this);
  }

  increment() {
    // NEVER DO THIS
    // console.log("click click", this);
    // this.state.count = this.state.count + 1;

    this.setState({
      count: this.state.count + 1,
      name: `${this.state.name}1`,
    });
  }
  */

  state = {
    count: this.props.initialCount,
    name: "Vince",
  };

  increment = () => {
    /*
    this does not work...
    console.log(`Current: Count is now ${this.state.count}`);
    this.setState({ count: this.state.count + 1 });
    console.log(`Second: Count is now ${this.state.count}`);
    this.setState({ count: this.state.count + 1 });
    console.log(`Final: Count is now ${this.state.count}`);
    */
    console.log(`this.state: ${this.state.count}`);

    // happenening after increment is called
    this.setState((currentState) => {
      // NEVER USE this.state.whatever in here
      // always use the argument as the current state
      console.log(`setState 1: ${currentState.count}`);
      return {
        count: currentState.count + 1,
      };
    });
    console.log(`this.state: ${this.state.count}`);

    // happenening after increment is called and alsp after the first setState
    this.setState((currentState) => {
      console.log(`setState 2: ${currentState.count}`);
      return {
        count: currentState.count + 1,
      };
    });
    console.log(`this.state: ${this.state.count}`);
  };

  render() {
    const { initialCount } = this.props;
    const { count, name } = this.state;
    // const name = this.state.name;
    // const count = this.state.count;
    return (
      <React.Fragment>
        <p>Current Props Count: {initialCount}</p>
        <p>Current State Count: {count}</p>
        <p>Hi my name is {name}</p>
        <button onClick={this.increment}>Increment</button>
      </React.Fragment>
    );
  }
}
