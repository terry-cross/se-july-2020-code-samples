// https://reactgo.com/format-date-time-javascript/
const dateOptions = {
  long: {
    // Saturday, December 8, 2018
    weekday: "long",
    year: "numeric",
    month: "long",
    day: "numeric",
  },
  short: {
    // Sat, Dec 08, 2018
    weekday: "short",
    year: "numeric",
    month: "short",
    day: "2-digit",
  },
  calendar: {
    // 12/08/18
    year: "2-digit",
    month: "2-digit",
    day: "2-digit",
  },
  precise: {
    // 8:09:19 AM
    hour12: true,
    hour: "2-digit",
    minute: "2-digit",
    second: "2-digit",
  },
  twentyFour: {
    // 13:42:31
    hour12: false,
    hour: "2-digit",
    minute: "2-digit",
    second: "2-digit",
  },
};

// ["long", "short", "calendar"......]
const dateFormats = Object.keys(dateOptions);

function formatDate(date, format = "precise") {
  const options = dateOptions[format] || dateOptions.precise;
  return date.toLocaleDateString("en-US", { ...options, timeZone: "Africa/Accra" });
}

// generate a random number with the min and max included
const randomNumber = (min, max) => Math.floor(Math.random() * (max - min + 1) + min);
