import express from "express";
import usersJSON from "./data/users.json";

// npm init esm <-- generates this cool scaffolding
// npm install express - install express to run your app
// npm instal -D nodemon - install nodemon to restart server on file changes
// start scrit - package.json - "node index.js"
// dev script - package.json - "nodemon index.js"

/*
GET - retreive data
POST - create an entity
PUT - replace an entity and all of its data
PATCH - update an entity with some or all of its data
DELETE - deletes an entity
*/
const app = express();
// we want to parse incoming JSON data if we have it
app.use(express.json());
// we want to parse information in our url
app.use(express.urlencoded({ extended: true }));

/*
req - all of the incoming information (the request) coming into our server
res - sending data back to the client that requested the information (sending a response)
*/
app.get("/", (req, res) => {
  res.send("Hello from my cool server!");
});

app.get("/users", (req, res) => {
  // res.json(usersJSON);
  // res.send(usersJSON);
  // sending JSON with a 200 status code
  // always call .status(Number) before send or json
  res.status(200).json(usersJSON);
});

app.post("/users", (req, res) => {
  // req.body is information from the requests body and you have access
  // to it (POST/PUT/PATCH - requests)
  const person = req.body;
  usersJSON.push(person);
  // 201 status code for creating things
  res.status(201).send("ok!");
});

// Should always be last because it matches EVERYTHING
// wildcard (*) matches EVERYTHING
app.get("*", (req, res) => {
  res.status(404).json({
    message: "You lost bruh??",
  });
});

// listen to the server on port 4000
app.listen(4000, () => {
  console.log("Express server is now running on port 4000");
});
